let temp2 = document.getElementById('tempo2');
let temp1 = document.getElementById('tempo1');

const imgContainers = document.getElementsByClassName('main__img-container');

console.log(imgContainers[0]);

if(temp2 && temp1) {
  temp2.addEventListener('click', () => {
    temp2.classList.add('tab__active');
    temp2.classList.remove('tab__inactive');
    temp1.classList.add('tab__inactive');
    temp1.classList.remove('tab__active');

    imgContainers[0].classList.add('active');
    imgContainers[0].classList.remove('inactive');
    imgContainers[1].classList.add('inactive');
    imgContainers[1].classList.remove('active');

  });
  
  temp1.addEventListener('click', () => {
    temp1.classList.add('tab__active');
    temp1.classList.remove('tab__inactive');
    temp2.classList.add('tab__inactive');
    temp2.classList.remove('tab__active');

    imgContainers[1].classList.add('active');
    imgContainers[1].classList.remove('inactive');
    imgContainers[0].classList.add('inactive');
    imgContainers[0].classList.remove('active');
  });

}

function getRickrolled() {
  window.location.href = "https://youtu.be/dQw4w9WgXcQ?si=3SW3zegtEtbARXD2";
}